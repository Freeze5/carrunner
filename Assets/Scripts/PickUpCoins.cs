﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PickUpCoins : MonoBehaviour {

    public int coinsCount;
    public Text coinTxt;

	void Start () {
		
	}
	
	
    void OnCollisionEnter ( Collision col)
    {
        if( col.gameObject.tag == "coins")
        {
            coinsCount++;
            coinTxt.text = "Coins: " + coinsCount.ToString();
            Destroy(col.gameObject);
        }
    }
}
