﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour {

    [SerializeField]
    private float _spawnDelay;

    [SerializeField]
    private PoolOfObjects _gameObjectPool;

    private float _timeBeforeSpawn ;

    public bool isSameDelay;
    public float minDelay;
    public float maxDelay;


    private void Start()
    {
        _timeBeforeSpawn = 0f;
        if (isSameDelay)
        {
            InvokeRepeating("Launch", _timeBeforeSpawn, _spawnDelay);
        }
        else
        {
            StartCoroutine(Launcher());
        }
    }

    IEnumerator Launcher()
    {
        yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
        Launch();
    }

    private void Launch()
    {
        var launch = _gameObjectPool.GetFromPool();
        launch.transform.position = transform.position;
        launch.transform.rotation = transform.rotation;
        launch.transform.SetParent(this.gameObject.transform);
        launch.SetActive(true);

        if (!isSameDelay)
        {
            StartCoroutine(Launcher());
        }
    }
}
