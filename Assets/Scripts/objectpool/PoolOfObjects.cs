﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolOfObjects : MonoBehaviour
{
    public static PoolOfObjects Instance { get; private set; }

    [SerializeField]
    private GameObject _prefab;

    public bool isRandomObject = false; // check if we wanna Inst one obj always or random obj from array
    
    [SerializeField]
    private GameObject[] _prefabs;

    private Queue<GameObject> _objectPool = new Queue<GameObject>();

    private void Awake()
    {
        Instance = this;
    }

    private void AddObjects( int objCount)
    {
        if (!isRandomObject)
        {
            var newObject = Instantiate(_prefab) as GameObject;
            newObject.SetActive(false);
            _objectPool.Enqueue(newObject);

            newObject.GetComponent<IObjectPooled>().Pool = this;
        }
        else
        {
            var newObject = Instantiate(_prefabs[Random.Range(0, _prefabs.Length)]);
            newObject.GetComponent<IObjectPooled>().Pool = this;
            newObject.SetActive(false);
            _objectPool.Enqueue(newObject);

            
        }
        
    }

    public GameObject GetFromPool()
    {
        if(_objectPool.Count == 0)
        {
            AddObjects(1);
        }
        return _objectPool.Dequeue();
    }

    public void ReturnToPool( GameObject objectToReturn)
    {
        objectToReturn.SetActive(false);
        _objectPool.Enqueue(objectToReturn);
    }

}
