﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovableObject : MonoBehaviour , IObjectPooled
{

    public Vector3 moveVector;

    public float speed;

    private float lifeTime;
    [SerializeField]
    private float maxLifeTime;

    public PoolOfObjects Pool { get; set; }


    private void OnEnable()
    {
        lifeTime = 0;
    }

    private void Update()
    {
        transform.Translate(moveVector * speed);
        lifeTime += Time.deltaTime;
        if (lifeTime >= maxLifeTime)
        {
            Pool.ReturnToPool(this.gameObject);
        }

    }
}

 interface IObjectPooled
{
    PoolOfObjects Pool { get; set; }
}
