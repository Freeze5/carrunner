﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is absolete and replaced by PoolManager.cs
/// </summary>
public class ObjectSpawner : MonoBehaviour {

    //интервалы
    public bool isSameDelay;//одинаковый
    public bool isRandomDelay;//разный
    //обьекты
    public GameObject ObjectToStawn;
    public GameObject[] ObjectsToSpawn;
    //время перед началом спавна
    public float timeBeforeSpawn;
    //промежутки в интервалах
    public float spawnDelay;
    public float minDelay;
    public float maxDelay;

	
	void Start () {
        if (isSameDelay)
        {
            InvokeRepeating("Spawn", timeBeforeSpawn, spawnDelay);
        }
        else
        {
            StartCoroutine(Spawner());
        }
	}
	
	
    IEnumerator Spawner()
    {
        yield return new WaitForSeconds(Random.Range(minDelay, maxDelay));
        Spawn();
    }

    void Spawn()
    {
        if (isRandomDelay)
        {
            GameObject obj = Instantiate(ObjectsToSpawn[Random.Range(0, ObjectsToSpawn.Length)], transform.position, transform.rotation);
        }
        if (!isRandomDelay)
        {
            GameObject obj = Instantiate(ObjectToStawn, transform.position, transform.rotation);
        }
        if (!isSameDelay)
            StartCoroutine(Spawner());
    }
}
