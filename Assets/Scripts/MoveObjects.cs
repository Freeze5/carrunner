﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script is absolete and replaced by MovableObject.cs
/// </summary>
public class MoveObjects : MonoBehaviour {

    public Vector3 moveVector;
    public float speed;

    public float timeToDestroy;


	void Start () {
        Destroy(this.gameObject, timeToDestroy);
	}
	
	
	void Update () {
        transform.Translate(moveVector * speed);
	}
}
