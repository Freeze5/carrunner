﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyBox : MonoBehaviour {

	void OnCollisionEnter( Collision col)
    {
        if( col.gameObject.name == "jeep")
        {
            SceneManager.LoadScene("Game Over");
        }
    }
}
