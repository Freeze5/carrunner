﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour {
    
    public Transform target;
    public Vector3 moveLeft;
    public Vector3 moveRight;
    public float speed;

    public bool isRight;
    public bool isLeft;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 movePos = new Vector3(target.transform.position.x, target.transform.position.y, -14.7f);

        if (Input.GetKey(KeyCode.A) || isLeft)
        {
            if(target.transform.position.x > -3.7f)
            {
                target.transform.Translate(moveLeft * Time.deltaTime * speed);
            }
        }

        if (Input.GetKey(KeyCode.D) || isRight)
        {
            if(target.transform.position.x < 3.7f)
            {
                target.transform.Translate(moveRight * Time.deltaTime * speed);
            }
        }
        this.transform.LookAt(target);
        this.transform.position = Vector3.MoveTowards(transform.position, movePos, Time.deltaTime * (speed - 0.5f));
	}

    public void TurnRight( bool isOn)
    {
        isRight = isOn;
    }
    public void TurnLeft(bool isOn)
    {
        isLeft = isOn;
    }
}
