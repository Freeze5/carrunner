﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCoin : MonoBehaviour {

    public float x;
    public float y;
    public float z;
	
	void Update () {
        transform.Rotate(x, y, z);
	}
}
